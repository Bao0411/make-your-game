class Explosion {
    constructor(x, y) {
        this.x = x
        this.y = y
        this.id = undefined
        this.image = "../images/explosions.png"
        this.width = 60
        this.height = 60
    }

    create() {
        let div = document.createElement('div')
        this.id = `explosion_${explosion_id}`
        div.setAttribute('id', this.id)
        explosion_id++
        if (explosion_id > 10) {
            explosion_id = 0
        }
        div.style.background = `url(${this.image}) 0px 0px`
        div.style.left = `${this.x-this.width/2}px`
        div.style.top = `${this.y-this.height/2}px`
        div.style.width = `${this.width}px`
        div.style.height = `${this.height}px`
        div.style.position = "absolute"
        div.style.zIndex = "2"
        document.body.appendChild(div)
        explosions.push(this)
        this.run()
    }

    delete() {
        let div = document.getElementById(this.id)
        div.remove()
        let result = []
        for (let i = 0; i < explosions.length; i++) {
            if (explosions[i].id === this.id) {
                result.push(explosions[i])
            }
        }
        explosions = result
    }

    run() {
        let last_date = new Date().getTime()/100
        let x = 0
        let y = 0
        const update = () => {
            let div = document.getElementById(this.id)
            let this_date = new Date().getTime()/100
            if (this_date - last_date > 0.4) {
                last_date = new Date().getTime()/100
                x -= this.width
                if (x === this.width * -4) {
                    x = 0
                    y -= this.height
                }
                if (y === this.height * -4) {
                    this.delete()
                    return
                }
                div.style.background = `url(${this.image}) ${x}px ${y}px`
            }
            window.requestAnimationFrame(update)
        }
        window.requestAnimationFrame(update)
    }

}