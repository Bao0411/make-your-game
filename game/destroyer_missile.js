class DestroyerMissile {
    constructor(x, y, id) {
        this.x = x
        this.y = y
        this.width = 16
        this.height = 16
        this.id = id
        this.image = "../images/big_bullet.png"
        this.deleted = false
    }

    create() {
        let div = document.createElement('div')
        div.setAttribute('id', this.id)
        div.style.left = `${this.x - 8}px`
        div.style.top = `${this.y}px`
        div.style.backgroundImage = `url(${this.image})`
        div.style.position = "absolute"
        div.style.width = `${this.width}px`
        div.style.height = `${this.height}px`
        div.style.zIndex = "2"
        document.body.appendChild(div)
        this.run()
    }

    collide(obj) {
        if (this.x > obj.x && this.x + this.width < obj.x + obj.width) {
            if (this.y + this.height > obj.y) {
                return true
            }
        }
        return false
    }

    check_shields() {
        for (let i = 0; i < shields.length; i++) {
            if (this.collide(shields[i])) {
                shields[i].update()
                let explosion = new Explosion(this.x, this.y)
                explosion.create()
                explosions.push(explosion)
                this.delete()
                return true
            }
        }
        return false
    }

    check_player() {
        let div = document.getElementById('player').getBoundingClientRect()
        if (this.collide(div)) {
            player.delete()
            game_on = false
            return true        
        }
        return false
    }

    delete() {
        this.deleted = true
        let div = document.getElementById(this.id)
        div.remove()
        let result = []
        for (let i =0; i<destroyer_missiles.length;i++) {
            if (destroyer_missiles[i].id !== this.id) {
                result.push(destroyer_missiles[i])
            }
        }
        destroyer_missiles = result
    }

    run() {
        let border = document.getElementById('border').getBoundingClientRect()
        let speed = 0.5
        const update = () => {
            if (game_on == false) {
                return
            }
            if (active === false) {
                console.log('destroyer missile prevented from moving due to pausing')
                return
            }
            if (this.deleted) {
                return
            }
            let div = document.getElementById(this.id)
            if (this.check_player()) {
                return
            }
            if (this.check_shields()) {
                return
            }
            if (this.y >= border.y + border.height) {
                this.delete()
                game_on = false
                win = false
                return
            }
            this.y += speed
            div.style.top = `${this.y}px`
            window.requestAnimationFrame(update)
        }
        window.requestAnimationFrame(update)
    }
}