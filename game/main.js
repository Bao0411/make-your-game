import {gameOver, startMenu} from "./menus.js"



function initialize() {
    startTime = new Date().getTime();
    let border = document.getElementById('border').getBoundingClientRect()
    player = new Player()
    player.initialize(border.x, border.y, border.width, border.height)
    let start_position_for_shield = border.x + border.width/2 - (amount_of_shields * 128)/2 + 32
    let y = border.y + border.height - 150
    for (let i = 0; i<amount_of_shields; i++) {
        let x = start_position_for_shield + 128 * i
        let shield = new Shield(x, y, `shield_${i}`)
        shield.initialize()
        shields.push(shield)
    }
    let space_x = 70
    let space_y = 95
    let x = -space_x
    y = 0
    for (let i = 0; i<15; i++) {
        x += space_x
        if (x == space_x * 5) {
            y += space_y
            x = 0
        }
        let enemy = new Enemy(border.x + 10 + x, border.y + 75 + y, `enemy_${i}`)
        enemy.create()
        enemies.push(enemy)
    }
    let destroyer1 = new Destroyer(border.x + 10, border.y + 20, 'destroyer_1')
    destroyer1.create()
    destroyers.push(destroyer1)

    let destroyer2 = new Destroyer(border.x + border.width - 520, border.y + 20, 'destroyer_2')
    destroyer2.create()
    destroyers.push(destroyer2)
    player.run()
    if ( !timerStarted ) {
        timer()
    }
    const update = () => {
        if (enemies.length === 0 && destroyers.length === 0) {
            game_on = false
            win = true
        }
        if (player_health === 0) {
            win = false
        }
        if (game_on === false) {
            for (let i = 0; i <shields.length; i++) {
                let div = document.getElementById(shields[i].id)
                div.remove()
            }
            for (let i = 0; i < enemies.length; i++) {
                let div = document.getElementById(enemies[i].id)
                div.remove()
            }
            for (let i = 0; i < destroyers.length; i++) {
                let div = document.getElementById(destroyers[i].id)
                div.remove()
            }
            for (let i = 0; i < destroyer_missiles.length; i++) {
                let div = document.getElementById(destroyer_missiles[i].id)
                div.remove()
            }
            enemies = []
            shields = []
            destroyers = []
            destroyer_missiles = []
            player = undefined
            gameOver(initialize, win)
            booted = true
            return
        }
        window.requestAnimationFrame(update)
    }
    window.requestAnimationFrame(update)
}
startMenu(initialize)

//below is the logic for pause/resume
//boolean 'active' is used to determine whether the game is paused or not
//if active is false, player.move() & missile.moveMissile() & bullet.move & enemy.move & destroyer.run & destroyer_missile.run return
 let modal = document.getElementById("myModal");
 let pauseblockVisible = false;
 let instructionsblockVisible = false;
 document.body.addEventListener('keydown', e => {
    if ( e.key === 'p' && game_on === true && !instructionsblockVisible) {
     if (active) {
         active = false
         console.log('pause button clicked, active = false');
         modal.style.display = "block";
         pauseblockVisible = true;

     } else {
         active = true;
         console.log('resume button clicked, active = true');
         modal.style.display = "none";
         player.run();
         missiles.forEach(e => e.moveMissile());
         bullets.forEach(e => e.move());
         enemies.forEach(e => e.move());
         destroyers.forEach(e => e.run());
         destroyer_missiles.forEach(e => e.run());
         pauseblockVisible = false;
     }
 }
 if (e.key === 'l' && active === false) {
    //use location.reload method
    location.reload();
    }
    });
//below is the logic for instructions, it will be accessible from the start menu and ingame also
    let instructions = document.getElementById("instructions");
    document.body.addEventListener('keydown', e => {
       if ( e.key === 'i' && !pauseblockVisible ) {
        if (active) {
            active = false
            console.log('instructions button clicked, active = false');
            instructions.style.display = "block";
            instructionsblockVisible = true;
        } else {
            active = true;
            console.log('instructions button clicked, active = true');
            instructions.style.display = "none";
            if (game_on) {
            player.run();
            }
            missiles.forEach(e => e.moveMissile());
            bullets.forEach(e => e.move());
            enemies.forEach(e => e.move());
            destroyers.forEach(e => e.run());
            destroyer_missiles.forEach(e => e.run());
            instructionsblockVisible = false;
        }
    }
})
let timerStarted = false;
function timer () {
    // Set the starting time of the timer in seconds
let time = 0;
timerStarted = true;
// Get the timer element from the HTML
const timer = document.getElementById("timer");

// Update the timer display every second
setInterval(() => {
    if (!active) {
        return
    }
  // Calculate the minutes and seconds
  const minutes = Math.floor(time / 60);
  const seconds = time % 60;

  // Add leading zeros to the minutes and seconds
  const formattedMinutes = String(minutes).padStart(2, "0");
  const formattedSeconds = String(seconds).padStart(2, "0");

  // Update the timer display
  timer.innerHTML = `${formattedMinutes}:${formattedSeconds}`;
    
  // Increment the time by 1 second
  time++;
}, 1000);
}







