//global ID for player animation RequestAnimationFrame
let upDatePlayerID
class Player {
    constructor() {
        this.image =  "../images/player_ship.png"
        this.projectiles = []
        this.last_bullet = new Date()
        this.last_missile = new Date()
        this.image_x = 0
        this.image_y = 0
    }
    
    initialize() {
        const player_div = document.getElementById("player")
        //setting standard values
        player_div.style.background = `url(${this.image}) 0px 0px`
        player_div.style.position = "absolute"
        player_div.style.width = "48px"
        player_div.style.height = "48px"
        player_div.style.zIndex = "2"
        //defining start x and y
        let border = document.getElementById("border").getBoundingClientRect()
        //moving player
        player_div.style.left = `${border.x + border.width/2 - 24}px`
        player_div.style.top = `${border.y + border.height - 50}px`
    }

    cool_down() {
      if (new Date().getTime()/1000 - this.last_bullet.getTime()/1000 > 0.5) {
        this.last_bullet = new Date()
        return true
      }
      return false
    }
    cool_missile() {
      if (new Date().getTime()/1000 - this.last_missile.getTime()/1000 > 2) {
        this.last_missile = new Date()
        return true
      }
      return false
    }

    shoot() {
      if (booted) {
        return
      }
      document.body.addEventListener('keydown', e => {
          let player = document.getElementById("player").getBoundingClientRect()
          if (game_on == false) {
            return
          }
          if (e.key === 'w' && this.cool_down()) {
            if (!active) {
              return
            }
            let bullet = new Bullet(player.x + player.width/2, player.y, 0, `bullet_${current_bullet_id}`)
  
            bullet.create()
            bullets.push(bullet)
            current_bullet_id += 1
            if (current_bullet_id === 11) {
              current_bullet_id = 0
            }
          }
        })
    }

    launch() {
      if (booted) {
        return
      }
      
      document.body.addEventListener('keydown', e => {
          let player = document.getElementById("player").getBoundingClientRect()
          if (e.key === 'r' && this.cool_missile()) {
            if (game_on == false) {
              return
            }
            if (!active) {
              return
            }
            let missile = new Missile(player.x + player.width/2, player.y, 0, `missile_${current_missile_id}`)
            missile.createMissile()
            missiles.push(missile)
            current_missile_id += 1
            if (current_missile_id === 11) {
              current_missile_id = 0
            }
          }
        })
    }

    delete() {
      let player = document.getElementById("player")
      player.remove()
      let div = document.createElement('div')
      div.setAttribute('id', 'player')
      document.body.appendChild(div)
    }

    update_image(movement) {
      let image = 6 - player_health
      let div = document.getElementById("player")
      let image_url = "../images/player_ship.png"
      if (movement != "idle") {
        //making sure we are using the sprite sheet and multiplying the image with two because this sprite sheet include doubled the original amount of images.
        image_url = "../images/side_view.png"
        image = image * 2
      }
      let x = 0
      let y = 0
      //if the ship is idle, we just want the correct deterioration of the ship in comparison to the current health
      if (movement == "idle") {
        for (let i = 0; i<image; i++) {
          x -= 48
          if (x == -48 *2) {
            y -= 48
            x = 0
          }
        }
        div.style.background = `url(${image_url}) ${x}px ${y}px`
        return
        //as the comment said above if the ship is idle we don't wanna do anything more past this point, that is why it returns
      }
      //if the ship is moving
      for (let i=0; i<image; i++) {
        x -= 48
          if (x == -48 *3) {
            y -= 48
            x = 0
          }
      }
      //if the ship is moving to the left, the coordinates will already be on the correct image
      if (movement === "left") {
        x -= 48
        if (x == -48 *3) {
            y -= 48
            x = 0
        }
      }
      div.style.background = `url(${image_url}) ${x}px ${y}px`
    }

    move() {
    

        let player = document.getElementById("player")
        let player_properties = player.getBoundingClientRect()
        let border = document.getElementById("border").getBoundingClientRect()
        let keydown = ''
        document.body.addEventListener('keydown', e => {
            keydown = e.key;
          });
          document.body.addEventListener('keyup', e => {
            keydown = '';
          });
          const update = () => {
            if (player_health === 0) {
              game_on = false
            }
            if (game_on === false) {
              this.delete()
              return
            }
            if (active === false) {
              console.log('player prevented from moving due to pausing')
              return
          }
         
            let speed = player_speed
              switch (keydown) {
              case 'a':
                this.update_image("left")
                if (player_properties.x <= border.x + 20 ) {
                  speed = 0
                  player_properties.x = border.x + 20
                }
                player_properties.x -= speed;
                player.style.left = `${player_properties.x}px`;
                break;
              case 'd':
                this.update_image("right")
                if (player_properties.x + player_properties.width >= border.x + border.width - 20) {
                  speed = 0
                  player_properties.x = border.x + border.width - player_properties.width - 20
                }                
                player_properties.x += speed;
                player.style.left = `${player_properties.x}px`;
                break;
              default:
                this.update_image("idle")
              }
            this.updatePlayerID = window.requestAnimationFrame(update)
        }
      this.updatePlayerID =  window.requestAnimationFrame(update)
    }
    
    run () {
      this.shoot()
      this.launch()
      this.move()
    }
    
}

