class Bullet {
    constructor(x, y, hostility, id) {
        this.x = x 
        this.y = y
        this.width = 5
        this.height = 20
        this.hostility = hostility
        this.id = id
        this.collision = false
    }

    create() {
        let div = document.createElement('div')
        div.setAttribute('id', this.id)
        //set class to bullet
        div.setAttribute('class', 'bullet')
        div.style.position = "absolute"
        div.style.backgroundColor = "white"
        div.style.width = "5px"
        div.style.height = "20px"
        div.style.left = `${this.x}px`
        div.style.top = `${this.y}px`
        div.style.zIndex = "2"
        document.body.appendChild(div)
        this.move()
    }

    delete() {
        if (this.collision === true) {
            let explosion = new Explosion(this.x, this.y)
            explosion.create()
        }
        let bullet = document.getElementById(this.id)
        bullet.remove()
        let result = []
        bullets.forEach(e => {
            if (e.id != this.id) {
                result.push(e)
            }
        })
        bullets = result
    }

    collide(obj) {
        if (this.x > obj.x && this.x + this.width < obj.x + obj.width) {
            if (this.y < obj.y + obj.height && this.hostility === 0) {
                this.collision = true
                return true
            }
            if (this.y + this.height > obj.y && this.hostility === 1) {
                this.collision = true
                return true
            }
        }
        return false
    }

    check_shields() {
        for (let i = 0; i<shields.length; i++) {
            if (this.collide(shields[i])) {
                let explosion = new Explosion(this.x, this.y)
                explosion.create()
                this.delete()          
                shields[i].update()
                return true
            }
        }
        return false
    }

    check_enemies() {
        for (let i = 0; i<enemies.length; i++) {
            if (this.hostility === 1) {
                break
            }
            if (this.collide(enemies[i])) {
                this.delete()
                enemies[i].update()
                return true
            }
        }
        return false
    }

    check_player() {
        let player = document.getElementById("player").getBoundingClientRect()
        if (this.collide(player)) {
            let explosion = new Explosion(this.x, this.y)
            explosion.create()
            this.delete()
            player_health--
            if (player_health === 0) {
                game_on = false
            }
            updateLives()
            return true
        }
        return false
    }

    move() {
        let border = document.getElementById("border").getBoundingClientRect()
        const update = () => {
            if (game_on === false) {
                this.delete()
                return
            }
            if (active === false) {
                console.log('bullet prevented from moving due to pausing')
                return
            }
            if (this.hostility === 1 && this.check_player()) {
                return
            }
            if (this.check_shields()) {
                return
            }
            if (this.check_enemies()) {
                let explosion = new Explosion(this.x, this.y)
                explosion.create()
                return
            }
            if (this.y < border.y || this.y + this.height > border.y + border.height) {
                this.delete()
                return
            }
            let bullet = document.getElementById(this.id)
            let speed = 20
            if (this.hostility == 1) {
                speed = -5
            }
            let y = bullet.getBoundingClientRect().y - speed
            this.y = y
            bullet.style.top = `${y}px`
            window.requestAnimationFrame(update)
        }
        window.requestAnimationFrame(update)
    }

}