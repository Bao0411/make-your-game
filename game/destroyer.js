class Destroyer {
    constructor(x, y, id) {
        this.x = x
        this.y = y
        this.id = id
        this.image = "../images/base_destroyer.png"
        this.loading = false
        this.width = 48
        this.height = 48
        this.level = 0
        this.last_shot = new Date()
        this.cool_down_sec = destroyer_cool_down_sec
        this.last_load_up = new Date()
        this.health = 2
        this.deleted = false
        this.direction = 1
        this.speed = .5
    }

    create() {
        let div = document.createElement('div')
        div.setAttribute('id', this.id)
        div.style.zIndex = "2"
        div.style.left = `${this.x}px`
        div.style.top = `${this.y}px`
        div.style.background = `url(${this.image}) 0px 0px`
        div.style.position = "absolute"
        div.style.width = `${this.width}px`
        div.style.height = `${this.height}px`
        document.body.appendChild(div)
        this.run()
    }

    update() {
        this.health -= 1
        if (this.health == 0) {
            this.delete()
        }
    }

    delete() {
        let div = document.getElementById(this.id)
        div.remove()
        let result = []
        this.deleted = true
        for (let i = 0; i < destroyers.length; i++) {
            if (destroyers[i].id !== this.id) {
                result.push(destroyers[i])
            }
        }
        destroyers = result
    }

    cool_down() {
        let last_time = this.last_shot.getTime()/1000
        let now = new Date().getTime()/1000
        
        if (now - last_time > this.cool_down_sec) {
            let num = Math.random() * 10
            if (num >2) {
                return true
            } else {
                this.last_shot = new Date()
            }
        }
        return false
    }

    load_up() {
        let div = document.getElementById(this.id)
        if (this.level == 13) {
            div.style.background = `url(${this.image}) 0px 0px`
            this.shoot()
            this.last_shot = new Date()
            this.level = 0
            this.loading = false
            return
        }
        let last_time = this.last_load_up.getTime()/1000
        let now = new Date().getTime()/1000
        if (now - last_time > 0.5) {
            this.last_load_up = new Date()
            let x = 0
            let y = 0
            for (let i = 0; i<this.level; i++) {
                x -= this.width
                if (x < this.width * -1 * 3) {
                    x = 0
                    y -= this.width
                }
            }
            this.level++
            div.style.background = `url(${this.image}) ${x}px ${y}px`
            }
        }
        shoot() {
            let obj = new DestroyerMissile(this.x + this.width/2, this.y + this.height, `destroyer_missile_${destroyer_missile_id}`)
            destroyer_missile_id++
            if (destroyer_missile_id > 8) {
            destroyer_missile_id = 0
            }
            obj.create()
            destroyer_missiles.push(obj)
        }
        
        run() {
            const update = () => {
                if (this.deleted === true) {
                    return
                }
                if (game_on == false) {
                    return
                }
                if (this.loading === false && this.cool_down()) {
                    this.loading = true
                } else if (this.loading === true) {
                    this.load_up()
                }
                let border = document.getElementById("border").getBoundingClientRect()
                if (this.x + this.width >= border.x + border.width || this.x <= border.x) {
                    this.direction *= -1;
                } else if (this.x <= 0 && this.direction === -1) {
                    this.direction = 1
                }
                // Ensure that the x coordinate stays within the desired boundary
                if (this.x + this.speed * this.direction >= 0 && this.x + this.speed * this.direction <= window.innerWidth - this.width) {
                    this.x += this.speed * this.direction
                }
                if (active === false) {
                console.log('destroyer prevented from moving due to pausing')
                return
            }
            let div = document.getElementById(this.id)
                div.style.left = `${this.x}px`
                window.requestAnimationFrame(update)
            }
            window.requestAnimationFrame(update)
        }
    }
        