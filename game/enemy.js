class Enemy {
    constructor (x, y, id) {
        this.x = x
        this.y = y
        this.id = id
        this.width = 48
        this.height = 48
        this.image = "../images/enemy.png"
        this.image_x = 0
        this.image_y = 0
        this.last_load_up = new Date()
        this.last_shot = new Date()
        this.loading = false
        this.health = 1
        this.direction = 1;
        this.speed = enemy_speed;
    }

    create() {
        let div = document.createElement('div')
        div.setAttribute('id', this.id)
        div.style.background = `url(${this.image}) ${this.image_x}px ${this.image_y}px`
        div.style.width = `${this.width}px`
        div.style.height = `${this.height}px`
        div.style.left = `${this.x}px`
        div.style.top = `${this.y}px`
        div.style.position = `absolute`
        div.style.zIndex = 2
        let border = document.getElementById("border").getBoundingClientRect()
        //moving player
        div.style.left = `${border.x + border.width/2 - 24}px`
        div.style.top = `${border.y + border.height - 50}px`
        document.body.appendChild(div)
        this.move()
    }


    update() {
        this.health -= 1
        let div = document.getElementById(this.id)
        if (this.health === 0) {
            div.remove()
            this.delete()
            return
        }
        this.image_x = 0
        this.image_y -= 48
        if (this.health === 1) {
            this.image_x -= 48
            this.image_y = 0
        }
        div.style.background = `url(${this.image}) ${this.image_x}px ${this.image_y}px`
    }

    delete() {
        let index = enemies.findIndex(enemy => enemy.id === this.id)
        if (index !== -1) {
          enemies.splice(index, 1)
          // Increase the score by 100 points
          let score = document.getElementById("score")
          score.innerHTML = "Score: " + (parseInt(score.innerHTML.split(":")[1]) + 100)
        }
    }       

    cool_down() {
        if (new Date().getTime()/1000 - this.last_shot.getTime()/1000 > 2) {
            let num = Math.random() * 10
            if (num > enemy_shooting_probability) {
                return true
            } else {
                this.last_shot = new Date()
            }
        }
        return false
    }

    load_up() {
        if (new Date().getTime()/1000 - this.last_load_up.getTime()/1000 > 0.2 && this.loading) {
            this.last_load_up = new Date()
            this.image_x -= this.width
            if (this.image_x == this.width * -4) {
                this.image_x = 0
                this.image_y -= this.height
            }
            if (this.image_y == this.height * -4 && this.image_x == this.width*-1)  {
                this.shoot()
                this.loading = false
                this.last_shot = new Date()
                this.image_x = 0
                this.image_y = 0
            }
            let div = document.getElementById(this.id)
            div.style.background = `url(${this.image}) ${this.image_x}px ${this.image_y}px`
        }
    }

    shoot() {
        let bullet = new Bullet(this.x, this.y, 1, `bullet_${current_bullet_id}`)
        bullet.create()
        current_bullet_id++
        if (current_bullet_id > 11) {
            current_bullet_id = 0
        }
        bullets.push(bullet)
    }

    move() {
        const update = () => {
            if (game_on == false) {
                return
            }
            if (this.health === 0) {
                return
            }
            if (active === false) {
                console.log('enemy prevented from moving due to pausing')
                return
            }
            if (this.loading === false) {
                this.loading = this.cool_down()
            }
            this.load_up()
            this.x += this.direction * this.speed
            let border = document.getElementById("border").getBoundingClientRect()
            if (this.x + this.width >= border.x + border.width || this.x <= border.x) {
                this.direction *= -1;
                this.y += this.height;
            }
            //check that if the enemy is at the bottom of the screen and colliding with the border, it will stop the game
            if (this.y + this.height >= border.y + border.height) {
                game_on = false
                active = false
                //remove all divs with the class missile (fixed bug: some would remain onscreen when defeated)
                let divs = document.getElementsByClassName("missile")
                for (let i = 0; i < divs.length; i++) {
                    divs[i].remove()
                }
                let div = document.getElementById("game_over")
                div.style.display = "block"
                return
            }
            let div = document.getElementById(this.id);
            div.style.left = `${this.x}px`;
            div.style.top = `${this.y}px`;
            window.requestAnimationFrame(update)
        };
        window.requestAnimationFrame(update);
    }    

}