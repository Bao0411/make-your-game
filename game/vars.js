var game_on = false,
destroyer_missiles = [],
destroyers = [],
enemies = [],
current_bullet_id = 0,
shields = []
player = undefined,
player_health = 6,
amount_of_shields = 4,
bullets = [],
destroyer_missile_id = 0
current_missile_id = 0,
missiles = [],
player_speed = 7,
explosions = [],
explosion_id = 0,
win = false,
enemy_shooting_probability = 9,
destroyer_cool_down_sec = 7,
enemy_speed = 1,
level = 0,
booted = false
//indicator if the game is on or not
var active = true
let startTime = 0;
let timerInterval = null;




function updateLives() {
    let livesDiv = document.getElementById('lives')
    if (livesDiv) {
        livesDiv.innerHTML = `Lives: ${player_health}`
    }
}


/*i want to add a timer that counts up and handles minutes. The timer should start when the game is started, should pause when the game is paused, resume when the game resumes, and reset when the game is restarted.*/