function increaseDifficulty() {
    if (player_speed > 3) {
        player_speed -= 0.5
    }
    enemy_speed += 0.5
    if (enemy_shooting_probability > 0) {
        enemy_shooting_probability -= 1
    }
    if (destroyer_cool_down_sec > 2) {
        destroyer_cool_down_sec -= 2.5
    }
}

function deleteMenu(id) {
    let div = document.getElementById(id)
    div.remove()
}

function createMenuBox(id) {
    let width = 400
    let height = 300
    let div = document.createElement('div')
    let border = document.getElementById('border').getBoundingClientRect()
    div.setAttribute('id', id)
    div.style.position = 'absolute'
    div.style.width = `${width}px`
    div.style.height = `${height}px`
    div.style.left = `${border.x + border.width/2 - width/2}px`
    div.style.top = `${border.y + border.height/2 - height/2}px`
    div.style.borderStyle = "solid"
    div.style.borderWidth = "5px"
    div.style.borderColor = "white"
    div.style.zIndex = "2"
    div.style.backgroundColor = "black"
    document.body.appendChild(div)
}

export function startMenu(start_game_function) {
    createMenuBox('start_menu')
    let text = document.createElement("h2")
    text.innerHTML = `Press ENTER to LOSE ;)<br><br> Press I for instructions`
    text.style.zIndex = "2"
    text.style.color = "white"
    text.style.textAlign = "center"
    let menu = document.getElementById('start_menu')
    menu.appendChild(text)
    let keydown = ''
    document.body.addEventListener('keydown', e => {
        keydown = e.key
    })
    const update = () => {
        if (keydown == "Enter") {
            game_on = true
            start_game_function()
            deleteMenu('start_menu')
            return
        }
        window.requestAnimationFrame(update)
    }
    window.requestAnimationFrame(update)
}

export function gameOver(start_game_function, win) {
    createMenuBox('game_over')

    let text = document.createElement("h2")
    text.innerHTML = "You LOST Hahhhahaa!!<br> Press ENTER to restart"
   
    active = false
    if (win === true) {
        console.log(win)
        active = false
        text.innerHTML = "You WON! Press ENTER <br>to continue to next level<br> <br> Press L to restart <br><br> Warning: you will lose progress on restart"
    }
    text.style.zIndex = "2"
    text.style.color = "white"
    text.style.textAlign = "center"
    let menu = document.getElementById('game_over')
    menu.appendChild(text)
    let keydown = ''
    document.body.addEventListener('keydown', e => {
        keydown = e.key
    })
    const update = () => {
        if (keydown === "Enter") {
            if (win) {
                deleteMenu('game_over')
                game_on = true
                active = true
                win = false
                level++
                increaseDifficulty()
                start_game_function()
                return
            } else {
                location.reload();
            }
        }
        if (keydown === 'l' && win) {
            location.reload();
        }
        window.requestAnimationFrame(update)
    }
    window.requestAnimationFrame(update)
}