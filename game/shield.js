class Shield {
    constructor (x, y, id) {
        this.x = x,
        this.y = y,
        this.image = "../../images/Shield.png"
        this.image_x = 0
        this.image_y = 0,
        this.width = 64,
        this.height = 64
        this.id = id
        this.health = 4
    }

    initialize() {
        let div = document.createElement('div')
        div.setAttribute('id', this.id)
        div.style.position = "absolute"
        div.style.background = `url(${this.image}) ${this.image_x}px ${this.image_y}px`
        div.style.width = `${this.width}px`
        div.style.height = `${this.height}px`
        div.style.top = `${this.y}px`
        div.style.left = `${this.x}px`
        div.style.zIndex = "2"
        document.body.appendChild(div)
    }

    delete() {
        let div = document.getElementById(this.id)
        div.remove()
        let result = []
        for (let i = 0; i<shields.length; i++) {
            if (shields[i].id !== this.id) {
                result.push(shields[i])
            }
        }
        shields = result
    }

    update() {
        this.health -= 1
        let div = document.getElementById(this.id)
        if (this.health === 0) {
            this.delete()
            return
        }
        this.image_x = 0
        this.image_y -= 64
        if (this.health === 3) {
            this.image_x -= 64
            this.image_y = 0
        }
        div.style.background = `url(${this.image}) ${this.image_x}px ${this.image_y}px`
    }
   
}
