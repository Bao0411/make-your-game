//let upDateMissile
class Missile {
    constructor(x, y, hostility, id) {
        this.x = x 
        this.y = y
        this.width = 5
        this.height = 20
        this.hostility = hostility
        this.id = id
    }

    createMissile() {
        let div = document.createElement('div')
        div.setAttribute('id', this.id)
        //set class to missile
        div.setAttribute('class', 'missile')
        div.style.position = "absolute"
        div.style.backgroundColor = "green"
        div.style.width = "5px"
        div.style.height = "20px"
        div.style.left = `${this.x}px`
        div.style.top = `${this.y}px`
        div.style.zIndex = "2"
        document.body.appendChild(div)
        this.moveMissile()
    }

    
    deleteMissile() {
        let div = document.getElementById(this.id)
        if (div === null) {
            return
        }
        div.remove()
        let result = []
        missiles.forEach(e => {
            if (e.id != this.id) {
                result.push(e)
            }
        })
        missiles = result
    }
    

    check_shields() {
        for (let i = 0; i<shields.length; i++) {
            if (this.x > shields[i].x && this.x < shields[i].x + shields[i].width) {
                if (this.y < shields[i].y + shields[i].height && this.hostility == 0) {
                    shields[i].update()
                    let explosion = new Explosion(shields[i].x + shields[i].width/2, shields[i].y + shields[i].height/2)
                    explosion.create()
                    this.deleteMissile()
                    return true
                }
            }
        }
        return false
    }

    check_shields_explosion() {
        for (let i=0; i<shields.length; i++) {
            if (this.y > shields[i].y && this.y < shields[i].y + shields[i].height) {
                shields[i].update()
                let explosion = new Explosion(shields[i].x + shields[i].width/2, shields[i].y + shields[i].height/2)
                explosion.create()
            }
        }
    }

    check_destroyer_missiles_explosion() {
        for (let i=0; i<destroyer_missiles.length; i++) {
            if (this.y > destroyer_missiles[i].y && this.y < destroyer_missiles[i].y + destroyer_missiles[i].height) {
                let explosion = new Explosion(destroyer_missiles[i].x + destroyer_missiles[i].width/2, destroyer_missiles[i].y + destroyer_missiles[i].height/2)
                explosion.create()
                destroyer_missiles[i].delete()
            }
        }
    }

    check_destroyer_explosion() {
        for (let i=0; i<destroyers.length; i++) {
            if (this.y > destroyers[i].y && this.y < destroyers[i].y + destroyers[i].height) {
                let explosion = new Explosion(destroyers[i].x + destroyers[i].width/2, destroyers[i].y + destroyers[i].height/2)
                explosion.create()
                destroyers[i].update()
            }
        }
    }

    beam() {
        let beam = document.createElement('div')
        beam.setAttribute('id', this.id + "_shock")
        beam.style.width = "10px"
        beam.style.height = "10px"
        beam.style.position = "absolute"
        beam.style.left = `${this.x}px`
        beam.style.top = `${this.y}px`
        beam.style.zIndex = "3"
        document.body.appendChild(beam)
        beam = document.getElementById(this.id + "_shock")
        let rgb_blue = 255
        beam.style.backgroundColor = `rgb(25,17,${rgb_blue})`
        let x = this.x
        let width = 10
        let border = document.getElementById("border").getBoundingClientRect()
        let speed = 20
        const update = () => {
            if (game_on === false) {
                beam.remove()
                return
            }
            this.check_destroyer_missiles_explosion()
            if (x + width > border.x + border.width && x < border.x) {
                beam.remove()
                return
            }
            width += speed
            if (x >= border.x) {
                x -= speed/2
            }
            if (x + width >= border.x + border.width) {
                width -= speed/2
            }
            rgb_blue -= 4
            beam.style.width = `${width}px`
            beam.style.left = `${x}px`
            beam.style.backgroundColor = `rgb(25, 17, ${rgb_blue})`
            window.requestAnimationFrame(update)
        }
        window.requestAnimationFrame(update)
    }

    explodeMissile() {
        let missile = document.getElementById(this.id)
        missile.style.backgroundColor = "red"
        missile.style.width = "1000px"
        missile.style.height = "2px"
        missile.style.left = `${this.x - 500}px`
        this.beam()
        this.check_shields_explosion()
        this.check_destroyer_explosion()
        this.deleteMissile()
    }

    moveMissile() {
        let border = document.getElementById("border").getBoundingClientRect()
        let exploded = false
        document.body.addEventListener('keydown', e => {
            if (e.key === 'q' && active === true) {
              missiles.forEach(e => e.explodeMissile())
              exploded = true
            }
        })
        const update = () => {
              if (active === false) {
                  console.log('missile prevented from moving due to pausing')
                  return
              }
            if (game_on === false) {
                this.deleteMissile()
                return
            }
            if (exploded) {
                return
            }
            if (this.check_shields()) {
                return
            }
            if (this.y + 20 < border.y) {
                this.deleteMissile()
                return
            }
            let missile = document.getElementById(this.id)
            let y = missile.getBoundingClientRect().y - 2
            this.y = y
            missile.style.top = `${y}px`
           this.upDateMissile = window.requestAnimationFrame(update)
        }
        this.upDateMissile = window.requestAnimationFrame(update)
    }


    runMissile() {
        this.moveMissile()
    }


    
}